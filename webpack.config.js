const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })

;

if (Encore.isProduction()) {
    Encore.configureFilenames({
        images: '[path][name].[hash:8].[ext]',
        fonts: '[path][name].[hash:8].[ext]'
    });
} else {
    Encore.configureFilenames({
        images: '[path][name].[ext]',
        fonts: '[path][name].[ext]'
    });
}

const config = Encore.getWebpackConfig();

module.exports = config;
