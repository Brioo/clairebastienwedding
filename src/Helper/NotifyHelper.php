<?php

namespace App\Helper;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class NotifyHelper
 *
 * @package App\Helper
 *
 * @author Quentin BENARD - Brio <benardq@gmail.com>
 */
class NotifyHelper
{
    /**
     * THe domain translation for notify
     */
    const TRANSLATION_NOTIFY_DOMAIN = 'notifications';

    /**
     * Level of notification to use
     */
    const NOTIFY_LEVEL_INFO = "info";
    const NOTIFY_LEVEL_SUCCESS = "success";
    const NOTIFY_LEVEL_WARNING = "warning";
    const NOTIFY_LEVEL_DANGER = "danger";

    /**
     * @param TranslatorInterface $translator
     *
     * @return array
     */
    public static function contactSuccessNotification(TranslatorInterface $translator): array
    {
        return [
            'type'      => self::NOTIFY_LEVEL_SUCCESS,
            'icon'      => 'far fa-smile-wink',
            'message'   => self::trans(
                $translator,
                sprintf('contact.form.%s.message',self::NOTIFY_LEVEL_SUCCESS)
            ),
            'title'     => self::trans(
                $translator,
                sprintf('contact.form.%s.title',self::NOTIFY_LEVEL_SUCCESS)
            ),
        ];
    }

    /**
     * @param TranslatorInterface $translator
     *
     * @return array
     */
    public static function contactErrorNotification(TranslatorInterface $translator): array
    {
        return [
            'type'      => self::NOTIFY_LEVEL_DANGER,
            'icon'      => 'fas fa-exclamation-circle',
            'message'   => self::trans(
                $translator,
                sprintf('contact.form.%s.message',self::NOTIFY_LEVEL_DANGER)
            ),
            'title'     => self::trans(
                $translator,
                sprintf('contact.form.%s.title',self::NOTIFY_LEVEL_DANGER)
            ),
        ];
    }

    /**
     * @param TranslatorInterface $translator
     * @param string $id
     * @param array $parameters
     *
     * @return string The translated string
     */
    public static function trans(TranslatorInterface $translator, string $id, array $parameters = [])
    {
        return $translator->trans($id, $parameters, self::TRANSLATION_NOTIFY_DOMAIN);
    }
}
