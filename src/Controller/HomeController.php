<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Helper\NotifyHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Serializer\FormErrorSerializer;
use Symfony\Component\Translation\TranslatorInterface;
use Datetime;

/**
 * Class HomeController
 *
 * @package App\Controller
 *
 * @author Quentin BENARD - Brio <benardq@gmail.com>
 */
class HomeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
    * @var FormErrorSerializer
    */
    private $formErrorSerializer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * HomeController constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormErrorSerializer $formErrorSerializer
     * @param TranslatorInterface $translator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormErrorSerializer $formErrorSerializer,
        TranslatorInterface $translator
    ) {
        $this->entityManager        = $entityManager;
        $this->formErrorSerializer  = $formErrorSerializer;
        $this->translator           = $translator;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $contact = new Contact();
        $contactForm = $this->createForm(ContactType::class, $contact);

        return $this->render('home/index.html.twig', [
            'controller_name'   => HomeController::class,
            'contactForm'       => $contactForm->createView(),
        ]);
    }

    /**
     * @Route("/evenement", name="event")
     */
    public function event(Request $request)
    {
        $contact = new Contact();
        $contactForm = $this->createForm(ContactType::class, $contact);

        return $this->render('home/event.html.twig', [
            'controller_name'   => HomeController::class,
            'contactForm'       => $contactForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/contact",
     *      name="contact",
     *      methods={"POST", "GET"},
     *      options={"expose"=true}
     * )
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contact(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $contact = new Contact();
            $contactForm = $this->createForm(ContactType::class, $contact);

            $contactForm->handleRequest($request);

            if ($contactForm->isSubmitted() && $contactForm->isValid()) {
                $this->entityManager->persist($contactForm->getData());
                $this->entityManager->flush();


                return new JsonResponse([
                    'status' => 'OK',
                    'notify' => NotifyHelper::contactSuccessNotification($this->translator),
                ],JsonResponse::HTTP_OK);
            }

            return new JsonResponse([
                'status'  => 'KO',
                'notify'  => NotifyHelper::contactErrorNotification($this->translator),
                'errors'  => $this->formErrorSerializer->convertFormToArray($contactForm),
            ], JsonResponse::HTTP_OK);
        }

        throw $this->createNotFoundException('Nothing to find here!');
    }
}
