<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181101101804
 *
 * @package DoctrineMigrations
 *
 * @author Quentin BENARD - Brio <benardq@gmail.com>
 */
final class Version20181101101804 extends AbstractMigration
{
    /**
     * We should start to check if table and column exist with schema->hasTable('eg')
     *
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('CREATE TABLE contact 
            ( 
                id      INT auto_increment NOT NULL, 
                name    VARCHAR(255) NOT NULL, 
                email   VARCHAR(255) NOT NULL, 
                phone   VARCHAR(255) NULL, 
                message VARCHAR(1000) NOT NULL, 
                PRIMARY KEY(id) 
            ) 
            DEFAULT CHARACTER SET utf8mb4 
            COLLATE utf8mb4_unicode_ci 
            engine = innodb '
        );
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE contact');
    }
}
