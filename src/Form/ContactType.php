<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactType
 *
 * @package App\Form
 *
 * @author Quentin BENARD - Brio <benardq@gmail.com>
 */
class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     *
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'     => false,
                'required'  => true,
                'attr'      => [
                    'placeholder' => 'home.contact.form.name.placeholder'
                ],
            ])
            ->add('email', TextType::class, [
                'label'     => false,
                'required'  => true,
                'attr'      => [
                    'placeholder' => 'home.contact.form.email.placeholder'
                ],
            ])
            ->add('phone', TelType::class, [
                'label'     => false,
                'required'  => false,
                'attr'      => [
                    'placeholder' => 'home.contact.form.phone.placeholder'
                ],
            ])
            ->add('message', TextareaType::class, [
                'label'     => false,
                'required'  => true,
                'attr'      => [
                    'placeholder' => 'home.contact.form.message.placeholder'
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Contact::class,
            'method'        => 'POST',
        ]);
    }
}
