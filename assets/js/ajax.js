(function($, Routing) {
    /**
     * Trigger contact form ajax request
     */
    $('form[name="contact"]').submit( function (e) {
        e.preventDefault();

        $.ajax({
            type: $(this).attr('method'),
            url: Routing.generate('contact'),
            data: $(this).serialize()
        })
            .done(function (data) {
                const notify = data.notify;

                if (data.status === 'KO') {
                    displayFormErrors('contact', data.errors);

                    $('.tooltip-contact-form')
                        .tooltipster({
                            animation: 'fade',
                            delay: 0,
                            theme: 'tooltipster-punk',
                            trigger: 'custom',
                            triggerOpen: {
                                mouseenter: true,
                                touchstart: true,
                            },
                            triggerClose: {
                                click: true,
                            },
                            functionInit: function(instance, helper) {
                                const $origin = $(helper.origin);
                                let dataOptions = $origin.attr('data-tooltipster');

                                if(dataOptions) {

                                    dataOptions = JSON.parse(dataOptions);

                                    $.each(dataOptions, function(name, option){
                                        instance.option(name, option);
                                    });
                                }
                            },
                        }).tooltipster('open');

                    notifyMe(notify);
                } else {
                    notifyMe(notify);
                }
            });
    });

    /**
     * @param notify
     */
    function notifyMe(notify) {
        $.notify({
            title: notify.title,
            icon: notify.icon,
            message: notify.message,
        }, {
            type: notify.type,
            delay: 15000

        });
    }

    /**
     * Handle the errors from fos bundle
     *
     * @param form
     * @param errors
     * @param field
     */
    function displayFormErrors(form, errors, field = null) {
        const errorsChildren = errors.children;

        if (field === null) {
            for (const prop in errorsChildren) {
                // skip loop if the property is from prototype
                if(!errorsChildren.hasOwnProperty(prop)) continue;

                if (typeof errorsChildren === 'object') {
                    // errorsChild should always be an array in our case
                    const child = errorsChildren[prop];

                    if(!child.hasOwnProperty('errors')) continue;

                    if (typeof child === 'object' && child !== undefined) {
                        const childErrors = child.errors;

                        if ($("#" + form + "_" + prop).hasClass('tooltipstered')) {
                            $('.tooltip-contact-form').tooltipster('destroy');
                        }

                        $("#" + form + "_" + prop).attr('title', childErrors.join('</br>'));
                    }
                }
            }
        }
    }
})(jQuery, Routing);
