// Load the entire content of Translations folder
const req = require.context("./static/translations/", true, /^(.*\.(js$))[^.]*$/im);
req.keys().forEach(function(key){
    req(key);
});
