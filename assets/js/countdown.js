/**
 * @param endtime
 *
 * @return {{total: number, days: number, months: *, hours: number, minutes: number, seconds: number}}
 */
function getTimeRemaining(endtime) {
    let t = Date.parse(endtime) - Date.parse(new Date());
    let seconds = Math.floor((t / 1000) % 60);
    let minutes = Math.floor((t / 1000 / 60) % 60);
    let hours = Math.floor((t / (1000 * 60 * 60)) % 24);

    let days = Math.floor(t / (1000 * 60 * 60 * 24));

    return {
        'total': t,
        'days': days,
        'months': monthDiff(new Date(), new Date('July 06, 2019 16:30:00')),
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

/**
 * @param d1
 * @param d2
 *
 * @return {*}
 */
function monthDiff(d1, d2) {
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();

    return months <= 0 ? 0 : months;
}

/**
 * @return {number}
 */
function getRemanningDays() {
    let dateFirst = new Date("July 01, 2019 16:30:00");
    let dateSecond = new Date("July 06, 2019 16:30:00");

    // time difference
    let timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());

    // days difference
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    return diffDays;
}

/**
 * @return {number}
 */
function getMonthDaysLeft(){
    let date = new Date();

    return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() - date.getDate();
}

/**
 * @param id
 * @param endtime
 */
function initializeClock(id, endtime) {
    let clock = document.getElementById(id);
    let monthsSpan = clock.querySelector('.months');
    let daysSpan = clock.querySelector('.days');
    let hoursSpan = clock.querySelector('.hours');
    let minutesSpan = clock.querySelector('.minutes');
    let secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        let t = getTimeRemaining(endtime);

        monthsSpan.innerHTML = t.months;
        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    let timeinterval = setInterval(updateClock, 1000);
}

let deadline = new Date(Date.parse(new Date('July 06, 2019 16:30:00')));

initializeClock('clockdiv', deadline);