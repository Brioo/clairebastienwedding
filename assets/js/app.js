// ./ (or ../) means to look for a local file
// loads the jquery package from node_modules

const $ = require('jquery');
global.$ = global.jQuery = $;

// Require images
require('./images');

// Require vendors
require('bootstrap');
require('jquery.easing');
require('tooltipster');
const WOW = require('wowjs');
window.wow = new WOW.WOW({
    live: false
});
window.inViewport = require('in-viewport');
window.Typed = require('typed.js');

// Fos Js routing solutions
const routes = require('./static/fos_js_routes.json');
const Routing = require('../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');
const Translator = require('../../vendor/willdurand/js-translation-bundle/Resources/js/translator.js');

global.Routing = Routing;
global.Translator = Translator;

Routing.setRoutingData(routes);
import 'owl.carousel';
// Require local scripts
require('./main');
require('./chart');
require('./formValidation');
require('./notify');
require('./ajax');
require('./translation');
require('./countdown');

// Require styles
require('../scss/app.scss');

$(document).ready(function() {
    window.wow.init();
    // welcomeNotify();
});

// /**
//  * Welcome notify to explain notify feature
//  */
// function welcomeNotify() {
//     $.notify({
//         animation: 'fade',
//         theme: 'tooltipster-punk',
//         title: Translator.trans('welcome.tips.1.title'),
//         message: Translator.trans('welcome.tips.1.message'),
//     }, {
//         delay: 5000,
//     });
// }
