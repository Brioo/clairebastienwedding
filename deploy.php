<?php

namespace Deployer;

require 'recipe/symfony4.php';

set('application', 'claireBastienWedding');
set('repository', exec('git config --get remote.origin.url'));
set('ssh_type', 'native');
set('allow_anonymous_stats', false);
set('deploy_path', '/var/www/claireBastienWedding');
set('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction --no-scripts');
set('keep_releases', 2);

task('deploy:assets:build', function () {
    run('cd {{release_path}} && yarn install');
    run('cd {{release_path}} && yarn encore production');
})->desc('Build assets via webpack');

task('deploy:server:reload', function () {
    $phpFmpVersion = getPhpFpmVersion();

    if (is_string($phpFmpVersion)) {
        run('sudo service apache2 reload');
        run('sudo service ' . $phpFmpVersion . ' reload');
    }
})->desc('Reloaded php-fpm');

task('deploy:fos:js-routing', function () {
    run(sprintf(
        'cd {{release_path}} && php bin/console fos:js-routing:dump --format=json --target=%s/assets/js/static/fos_js_routes.json',
        get('release_path'))
    );
})->desc('Generated Fos js routing');

task('deploy:js-translations', function () {
    run(sprintf(
        'cd {{release_path}} && php bin/console bazinga:js-translation:dump %s/assets/js/static/ --format=js',
        get('release_path'))
    );
})->desc('Generated js translations');

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:js-translations',
    'deploy:fos:js-routing',
    'deploy:assets:build',
    'deploy:cache:clear',
    'deploy:symlink',
    'deploy:copy_dirs',
    'deploy:unlock',
    'cleanup',
]);

after('deploy', 'deploy:server:reload');
after('deploy:failed', 'deploy:unlock');

/**
 * @return bool|null|string
 */
function getPhpFpmVersion() {
    $services = shell_exec('sudo service --status-all');
    $strPos = strpos($services, 'fpm');

    if ($strPos > 0) {
        return substr($services, $strPos - 7, 10);
    }

    return null;
}